use toy_rsa_lib::*;

pub const E: u64 = 65_537; //fixed value for RSA 'e'

pub fn lambda(p: u64, q: u64) -> u64 {
    lcm(p - 1, q - 1)
}

//Citation for Range assert: https://doc.rust-lang.org/std/ops/struct.Range.html
pub fn genkey() -> (u32, u32) {
    let mut p: u32 = rsa_prime();
    let mut q: u32 = rsa_prime();

    let mut lambda_n = lambda(p as u64, q as u64);
    let mut g_c_d: u64 = gcd(E, lambda_n);

    while E > lambda_n || g_c_d != 1 {
        p = rsa_prime();
        q = rsa_prime();
        lambda_n = lambda(p as u64, q as u64);
        g_c_d = gcd(E, lambda_n);
    }
    (p, q)
}

//key will be the result of p*q, passed in
pub fn encrypt(key: u64, msg: u32) -> u64 {
    let expanded: u64 = msg as u64;
    assert!(key != 0);
    let encrypted: u64 = modexp(expanded, E, key);
    encrypted
}

pub fn decrypt(key: (u32, u32), msg: u64) -> u32 {
    let p: u64 = key.0 as u64;
    let q: u64 = key.1 as u64;
    let l = lambda(p, q);
    let d = modinverse(E, l);
    let n = p * q;

    let decrypted = modexp(msg, d, n);
    u32::try_from(decrypted).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;

    #[test]
    fn random_msg() {
        for _ in 0..100 {
            let (p, q) = genkey();
            let pbig: u64 = u64::from(p);
            let qbig: u64 = u64::from(q);

            let mut rng = rand::thread_rng(); //create rng to use gen() function
            let key: u64 = pbig * qbig;
            let msg = rng.gen::<u32>(); //generate a random u32

            let encrypted = encrypt(key, msg); //send in msg as u32
            let decrypted = decrypt((p, q), encrypted);

            assert_eq!(msg, decrypted);
        }
    }

    #[test]
    fn msg_0() {
        let (p, q) = genkey();
        let pbig: u64 = u64::from(p);
        let qbig: u64 = u64::from(q);

        let key: u64 = pbig * qbig;
        let msg = 0;

        let encrypted = encrypt(key, msg); //send in msg as u32
        let decrypted = decrypt((p, q), encrypted);

        assert_eq!(msg, decrypted);
    }

    #[test]
    fn max_msg_size() {
        let (p, q) = genkey();
        let pbig: u64 = u64::from(p);
        let qbig: u64 = u64::from(q);

        let key: u64 = pbig * qbig;
        let msg = u32::MAX;

        let encrypted = encrypt(key, msg); //send in msg as u32
        let decrypted = decrypt((p, q), encrypted);

        assert_eq!(msg, decrypted);
    }
}
