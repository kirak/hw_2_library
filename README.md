# Kira Klingenberg
## Homework 2: Toy RSA

Run tests from lib.rs: cargo test
Run main from main.rs: cargo run

4/20/2023

Today I'm writing the gen_key function. To get p and q I know I can use Bart's provided rsa_prime() function.
The tricky bit seems to be generating two primes that meet the conditions for the generation loop.
rsa_prime() returns a u32, but lcm() and gcd(), both of which are needed for the loop logic, accept u64s.
The first thing I am trying is to safe cast the u32's to u64s, as suggested by the compiler. 
It doesn't look like the loop is necessary, in my test runs rsa_prime only seems to generate appropriate primes.
I guess it's good to have the check-loop anyways.

Next up, write the encrypt function.
This one takes in a key, represented by the result of p*q, cast as a u64, and a u32 msg number.
It will use the modexp and value of E provided by Bart to encrypt the message.
Ah ok now I see the issue with possible overflow in gen_key(). 
The p and q generated need to be small enough that they can fit in a u64 when multiplied together.
Otherwise the encrypt function can't accept a u64 as a key value (p*q).
Ok got that fixed, but we still panic.
I tracked it down to the all to modexp in the encrypt() function. 
Looks like we overflow when trying to raise the message to the E power, and this overflows a u64.

Just learned from this reddit post: https://www.reddit.com/r/learnrust/comments/rknznz/multiplication_overflow/
that using the (p*q) as u64, is multiplying p and q into a u32, then trying to convert that result to a u64.
So the overflow happens when we try to do the multiplication into a u32. 
I'll try converting p and q to u64 first.

Ah now running into the same error as above for the decrypt function. 
Rust panics on my attempt to raise msg (a u64) to the power of d(a u32). 
It apparently overflows during the multiplication.
I have tried increasing the size of msg to a u128, to no avail.
I have been using the pow() function, and attempted to use overflowing_pow and checked_pow earlier, both reported overflow.
Since d is going to be large, I'm not sure what to do here, unless I keep expanding msg?

Well that was the dumbest thing ever, I missed that msg^d mod n was just modexp(msg, d,n).
Called that function with the values I calculated and voila, it works!
Thanks to Sayer N on Zulip for saving me from a world of hurt there trying to write my own modexp unnecessarily.
Everything works on at least one input now, with no clippy warnings or compiler yelling.
I'm calling it a night and will write tests tomorrow!

4/21/2023

I just realized I can abstract out the calculation of lambda (p,q) to a function, implementing now.
Also making the change to use u32::try_from(value).unwrap() when shrinkinga  u64 to a u32.
the valuesof p and q in genkey need to stay as u32's so we can return them as u32s.
So I expand them only when sending them to the internal calculations (using the 'as u64' keywording).

4/24/2023

I went in and took out the check for an overflowing multiply, since that proved to be unnecessary.
I also attemped to get an assert! in there to check if the values coming back from rsa_prime indeed were calculated correctly.
However I ran into issues finding the right assert. 
I tried an assert_equals and an assert_matches, but kept running into rabbit hole errors.
Since this needs to get in tonight, and my kiddo keeps waking up, I am hedging my bets and just turning in what I have without these asserts.
I also did make a main.rs, so I included it with my submission ZIP. :)
